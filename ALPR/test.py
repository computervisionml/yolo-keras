from LicensePlateClass import LicensePlate, LicensePlate_V2

import argparse
import os

import cv2
from PIL import Image

from math import *
import os
import psutil

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='parse the image locations')
    parser.add_argument('-src',
                        metavar='s',
                        type=str,
                        default="./samples/test6.jpg",
                        help='the path to source of the image')

    args = parser.parse_args()
    test_image = args.src
    
    license_plate = LicensePlate_V2(saved_model="best_accuracy.pth", cuda=False)
    image = cv2.imread(test_image)
    plates = license_plate.fetch_plate_number(image)
    print(plates)