from craft_text_detector import read_image, load_craftnet_model
from craft_text_detector import load_refinenet_model, get_prediction
from craft_text_detector import export_detected_regions, export_extra_results

import numpy as np

import cv2
from PIL import Image

from math import *
import os
import psutil

import Detect
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='parse the image locations')
    parser.add_argument('-src',
                        metavar='s',
                        type=str,
                        default="./samples/test.jpeg",
                        help='the path to source of the image')
    parser.add_argument('-dst1',
                        metavar='d',
                        type=str,
                        default="./result/cropped_image_test.jpg",
                        help='the path to source of the image')
    parser.add_argument('-dst2',
                        metavar='d',
                        type=str,
                        default="./result/detected_text_test.jpg",
                        help='the path to source of the image')
    args = parser.parse_args()
    test_object = Detect.CRAFT()
    image = read_image(args.src)
    bounding_boxes = test_object.detect_text(image)
    for partImg, box in test_object.yield_image_crops(image, bounding_boxes):
        cv2.polylines(image, [box.astype(np.int32).reshape((-1, 1, 2))], True, color=(0, 0, 255), thickness=2)
        cv2.imwrite(args.dst1, partImg)
        cv2.imwrite(args.dst2, image)