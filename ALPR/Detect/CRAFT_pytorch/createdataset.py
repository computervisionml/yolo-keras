from craft_text_detector import read_image, load_craftnet_model
from craft_text_detector import load_refinenet_model, get_prediction
from craft_text_detector import export_detected_regions, export_extra_results

import numpy as np

import cv2
from PIL import Image
from os import listdir
from os.path import isfile, join
import random

from math import *
import os
import psutil

import Detect
import argparse
import glob


test_object = Detect.CRAFT()

gt = []

for subdir in ["br/", "eu/", "us/"]:
    folder = "./endtoend/"+subdir
    files =  glob.glob(folder+"/*.jpg")
    for file in files:
        print(file)
        image = cv2.imread(file)
        f = open(file.replace(".jpg", ".txt"), 'r')
        file_contents = f.read()
        name, xmin, ymin, xmax, ymax, alpr = tuple(file_contents.split("\t"))
        bounding_boxes = test_object.detect_text(image)


        i = 0
        for partImg, box in test_object.yield_image_crops(image, bounding_boxes):
            cv2.imwrite("./crops/"+subdir+str(i)+name, partImg)
            gt.append("./crops/"+subdir+str(i)+name+"\t"+alpr.replace("\n",""))
            i+=1
            
        

with open('gt.txt', 'w') as f:
    for item in gt:
        f.write("%s\n" % item)