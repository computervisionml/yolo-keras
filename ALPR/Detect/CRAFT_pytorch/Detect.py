import torch
from craft_text_detector import read_image, load_craftnet_model
from craft_text_detector import load_refinenet_model, get_prediction
from craft_text_detector import export_detected_regions, export_extra_results

import numpy as np

import cv2
from PIL import Image

from math import *
import os
import psutil

class CRAFT:
    """ 
    CRAFT class to detect and straighten number plate crops from a given image
    """

    def __init__(self, cuda = None):
        if cuda is None:
            self.CUDA_AVAILABLE = torch.cuda.is_available()
        else:
            self.CUDA_AVAILABLE = cuda
        
        self.refine_net = load_refinenet_model(cuda=self.CUDA_AVAILABLE)
        self.craft_net = load_craftnet_model(cuda=self.CUDA_AVAILABLE)

    def order_points(self, pts):
        '''
        Order the points in top-left, top-right, bottom-right, and bottom-left order for 4 point perspective transform
        
        Args:
        pts (list): list of four points specifying the (x, y) coordinates of each point of the rectangle.
        
        Returns:
        rect (list): ordered list of rectangle points
        '''
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = "float32")
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # return the ordered coordinates
        return rect
    
    def four_point_transform(image, pts):
        '''
        4 point perspective transform
        
        Args:
        image (np.ndarray): cv2 image
        pts (list): list of four points specifying the (x, y) coordinates of each point of the rectangle.
        
        Returns:
        warped (np.ndarray): straigtened cv2 image.  
        '''
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
        # return the warped image
        return warped

    def straighten_image(self, img, degree, pt1, pt2, pt3, pt4):
        '''
        Args:
            img (np.ndarray): license plate crop from the CRAFT model.
            degree (float): angle by which image needs to straigtened
            pt1 (Tuple): top left coordinate of the crop from source image
            pt2 (Tuple): top right coordinate of the crop from source image
            pt3 (Tuple): bottom right coordinate of the crop from source image
            pt4 (Tuple): bottom left coordinate of the crop from source image

        Returns
            imgOut (np.ndarray): straigtened license plate crop 
        '''
        height, width, _ = img.shape
        fabs_cos, fabs_sin = fabs(cos(radians(degree))), fabs(sin(radians(degree)))
        height_new = int(width * fabs_sin + height * fabs_cos)
        width_new = int(height * fabs_sin + width * fabs_cos)
        mat_rotation = cv2.getRotationMatrix2D((width / 2, height / 2), degree, 1)
        mat_rotation[0, 2] += (width_new - width) / 2
        mat_rotation[1, 2] += (height_new - height) / 2
        img_rotation = cv2.warpAffine(img, mat_rotation, (width_new, height_new), borderValue=(255, 255, 255))
        pt1 = list(pt1)
        pt3 = list(pt3)

        [[pt1[0]], [pt1[1]]] = np.dot(mat_rotation, np.array([[pt1[0]], [pt1[1]], [1]]))
        [[pt3[0]], [pt3[1]]] = np.dot(mat_rotation, np.array([[pt3[0]], [pt3[1]], [1]]))
        output_image = img_rotation[int(pt1[1]):int(pt3[1]), int(pt1[0]):int(pt3[0])]
        return output_image

    def sort_poly(self, p):
        '''
        Returns the clockwise sorted points strating from top left point to bottom left
        Args:
            p (np.ndarray): numpy array of dimension (2, 4) containing the coordinates
                of the vertices of the craft detection output.
        Returns
            a np.ndarray of shape (2, 4) containing the 4 points sorted clockwise
        '''
        min_axis = np.argmin(np.sum(p, axis=1))
        p = p[[min_axis, (min_axis+1)%4, (min_axis+2)%4, (min_axis+3)%4]]
        if abs(p[0, 0] - p[1, 0]) > abs(p[0, 1] - p[1, 1]):
            return p
        else:
            return p[[0, 3, 2, 1]]
    
    def yield_image_crops(self, image, bounding_boxes):
        '''
        Returns the straqightened crop of license plate and the original quadrilateral 
            bounding box coordinates w.r.t original image.
        Args:
            image (np.ndarray): original 3 channel cv2 image that was passed to the text detector.
            bounding_boxes (np.ndarray): np array of shape (2, 4) containing the vertex 
                coordinates of the initial detection
        '''
        for i, box in enumerate(bounding_boxes):
            box = self.sort_poly(box.astype(np.int32))
            pt1 = (box[0, 0], box[0, 1])
            pt2 = (box[1, 0], box[1, 1])
            pt3 = (box[2, 0], box[2, 1])
            pt4 = (box[3, 0], box[3, 1])
            partImg = self.straighten_image(image, degrees(atan2(pt2[1] - pt1[1], pt2[0] - pt1[0])), pt1, pt2, pt3, pt4)
            yield partImg, box
        
    def detect_text(self, image, text_threshold = 0.9, link_threshold = 0.2, low_text = 0.4, long_size = 1280):
        '''
        Returns the bounding boxes for all the text in the image argument.
        Args:
            image (np.ndarray): original 3 channel cv2 image from which text is to be detected
            text_threshold  (float): TODO_
            link_threshold  (float): TODO_
            low_text        (float): TODO_
            long_size       (float): TODO_
        '''
        prediction_result = get_prediction( image=image, 
                                            craft_net = self.craft_net, 
                                            refine_net = self.refine_net,
                                            text_threshold = text_threshold,
                                            cuda = self.CUDA_AVAILABLE,
                                            link_threshold = link_threshold,
                                            low_text = low_text,
                                            long_size = long_size )
        return prediction_result["boxes"]