import cv2
from PIL import Image
import imutils

import numpy as np

from CRAFT_keras import pipeline, detection, tools

import time
import os
import psutil
from math import *

'''
CRAFT Keras
'''
class CRAFT:
    """ 
    CRAFT class to detect and straighten number plate crops from a given image
    """
    def __init__(self):
        self.pipeline = pipeline.Pipeline()
    
    def order_points(self, pts):
        '''
        Order the points in top-left, top-right, bottom-right, and bottom-left order for 4 point perspective transform
        
        Args:
        pts (list): list of four points specifying the (x, y) coordinates of each point of the rectangle.
        
        Returns:
        rect (list): ordered list of rectangle points
        '''
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = "float32")
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # return the ordered coordinates
        return rect
    
    def four_point_transform(image, pts):
        '''
        4 point perspective transform
        
        Args:
        image (np.ndarray): cv2 image
        pts (list): list of four points specifying the (x, y) coordinates of each point of the rectangle.
        
        Returns:
        warped (np.ndarray): straigtened cv2 image.  
        '''
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
        # return the warped image
        return warped
    
    def straighten_image(self, img, degree, pt1, pt2, pt3, pt4):
        '''
        Args:
            img (np.ndarray): license plate crop from the CRAFT model.
            degree (float): angle by which image needs to straigtened
            pt1 (Tuple): top left coordinate of the crop from source image
            pt2 (Tuple): top right coordinate of the crop from source image
            pt3 (Tuple): bottom right coordinate of the crop from source image
            pt4 (Tuple): bottom left coordinate of the crop from source image
        Returns
            imgOut (np.ndarray): straigtened license plate crop 
        '''
        height, width, _ = img.shape
        fabs_cos, fabs_sin = fabs(cos(radians(degree))), fabs(sin(radians(degree)))
        height_new = int(width * fabs_sin + height * fabs_cos)
        width_new = int(height * fabs_sin + width * fabs_cos)
        mat_rotation = cv2.getRotationMatrix2D((width / 2, height / 2), degree, 1)
        mat_rotation[0, 2] += (width_new - width) / 2
        mat_rotation[1, 2] += (height_new - height) / 2
        img_rotation = cv2.warpAffine(img, mat_rotation, (width_new, height_new), borderValue=(255, 255, 255))
        pt1 = list(pt1)
        pt3 = list(pt3)

        [[pt1[0]], [pt1[1]]] = np.dot(mat_rotation, np.array([[pt1[0]], [pt1[1]], [1]]))
        [[pt3[0]], [pt3[1]]] = np.dot(mat_rotation, np.array([[pt3[0]], [pt3[1]], [1]]))
        output_image = img_rotation[int(pt1[1]):int(pt3[1]), int(pt1[0]):int(pt3[0])]
        return output_image


    def sort_poly(self, p):
        '''
        Returns the clockwise sorted points strating from top left point to bottom left
        Args:
            p (np.ndarray): numpy array of dimension (2, 4) containing the coordinates
                of the vertices of the craft detection output.
        Returns
            a np.ndarray of shape (2, 4) containing the 4 points sorted clockwise
        '''
        min_axis = np.argmin(np.sum(p, axis=1))
        p = p[[min_axis, (min_axis+1)%4, (min_axis+2)%4, (min_axis+3)%4]]
        if abs(p[0, 0] - p[1, 0]) > abs(p[0, 1] - p[1, 1]):
            return p
        else:
            return p[[0, 3, 2, 1]]

    def sort_crops(self, crops_dict):
        '''
        Sorts dictory based on key values
        Args:
            crops_dict (dictionary): a python dictionaryto be sorted based on it's key values
        
        Returns
            new_dict (dictionary): sorted dictionary
        '''
        new_dict = {}
        for key in sorted(crops_dict):
            new_dict[key] = crops_dict[key]
        return new_dict

    def stitch_crops(self, crops_dict):
        '''
        Sitches the image crops based on the sorted bounding box co-ordinates. 
        Args:
            crops_dict (dictionary): a sorted dictionary with the image crops detected in the original image
        
        Returns
            stitched_img (np.ndarray): stictched licence plate image
        '''
        stitched_img = None
        for key in crops_dict:
            if stitched_img is None:
                stitched_img = crops_dict[key]
                continue
            stitched_img = np.concatenate((stitched_img, crops_dict[key]), axis=1)
        return stitched_img
    
    def detect_text(self, frame):
        '''
        Detects text in an input image
        Args:
            frame (np.ndarray): an input image on which text detection has to be done
        
        Returns
            stitched_img (np.ndarray): stictched licence plate image
            bounding_boxes (np.ndarray): a numpy array containing the bounding box co-ordinates of all text detected in frame 
            
        '''
        image, _ = tools.resize_image(frame, max_scale=1, max_size=640)
        original_image = image.copy()
        images = [image]
        start = time.time()
        prediction_groups = self.pipeline.recognize_image(images)
        print("Time for text detection: ", time.time() - start)
        bounding_boxes = prediction_groups[0]
        crops_dict = {}
        for box in bounding_boxes:
            # cv2.polylines(original_image, [box.astype(np.int32).reshape((-1, 1, 2))], True, color=(0, 0, 255), thickness=2)
            box = self.sort_poly(box.astype(np.int32))
            pt1 = (box[0, 0], box[0, 1])
            pt2 = (box[1, 0], box[1, 1])
            pt3 = (box[2, 0], box[2, 1])
            pt4 = (box[3, 0], box[3, 1])
            crop_img = self.straighten_image(image, degrees(atan2(pt2[1] - pt1[1], pt2[0] - pt1[0])), pt1, pt2, pt3, pt4)
            # using imutuis to fix only height and maintain aspect ratio.
            crop_img_resize = imutils.resize(crop_img, height=50)
            crops_dict[box[0,0]] = crop_img_resize
            
        sorted_crops = self.sort_crops(crops_dict)
        stitched_img = self.stitch_crops(sorted_crops)
        return stitched_img, bounding_boxes