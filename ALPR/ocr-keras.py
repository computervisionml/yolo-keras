import matplotlib.pyplot as plt
import cv2
import numpy as np
import keras_ocr
import imutils
from keras_ocr import tools
import time
import os
import psutil
from math import *
from PIL import Image
import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)



class ANPR:
    def __init__(self):
        self.pipeline = keras_ocr.pipeline.Pipeline()

    def straighten_image(self, img, degree, pt1, pt2, pt3, pt4):
        height, width, _ = img.shape
        fabs_sin, fabs_cos = fabs(sin(radians(degree))), fabs(cos(radians(degree)))
        height_new = int(width * fabs_sin + height * fabs_cos)
        width_new = int(height * fabs_sin + width * fabs_cos)
        mat_rotation = cv2.getRotationMatrix2D((width / 2, height / 2), degree, 1)
        mat_rotation[0, 2] += (width_new - width) / 2
        mat_rotation[1, 2] += (height_new - height) / 2
        img_rotation = cv2.warpAffine(img, mat_rotation, (width_new, height_new), borderValue=(255, 255, 255))
        pt1 = list(pt1)
        pt3 = list(pt3)

        [[pt1[0]], [pt1[1]]] = np.dot(mat_rotation, np.array([[pt1[0]], [pt1[1]], [1]]))
        [[pt3[0]], [pt3[1]]] = np.dot(mat_rotation, np.array([[pt3[0]], [pt3[1]], [1]]))
        img_out = img_rotation[int(pt1[1]):int(pt3[1]), int(pt1[0]):int(pt3[0])]
        height, width = img_out.shape[:2]
        return img_out

    def sort_poly(self, p):
        min_axis = np.argmin(np.sum(p, axis=1))
        p = p[[min_axis, (min_axis+1)%4, (min_axis+2)%4, (min_axis+3)%4]]
        if abs(p[0, 0] - p[1, 0]) > abs(p[0, 1] - p[1, 1]):
            return p
        else:
            return p[[0, 3, 2, 1]]

    def draw_text(self, img, text,
            pos=(0, 0),
            font=cv2.FONT_HERSHEY_DUPLEX,
            font_scale=0.5,
            font_thickness=2,
            text_color=(0, 255, 255),
            text_color_bg=(0, 0, 0)
            ):

        x, y = pos
        text_size, _ = cv2.getTextSize(text, font, font_scale, font_thickness)
        text_w, text_h = text_size
        cv2.rectangle(img, pos, (x + text_w, y + text_h), text_color_bg, -1)
        cv2.putText(img, text, (x, y + text_h), font, font_scale, text_color, font_thickness)
        return img

    def recognition(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame,_ = tools.resize_image(frame, max_scale=1, max_size=640)
        images = [frame]
        prediction_groups = self.pipeline.recognize(images)
        for group in prediction_groups[0]:
            # get text
            text = group[0]
            text = text.upper()
            num_digits = sum(list(map(lambda x:1 if x.isdigit() else 0,set(text))))
            if num_digits<1:
                continue
            # get bbox
            box = group[1]
            box = self.sort_poly(box.astype(np.int32))
            pt1 = (box[0, 0], box[0, 1])
            pt2 = (box[1, 0], box[1, 1])
            pt3 = (box[2, 0], box[2, 1])
            pt4 = (box[3, 0], box[3, 1])

            cv2.polylines(frame, [box.astype(np.int32).reshape((-1, 1, 2))], True, color=(0, 0, 255), thickness=2)
            bottomLeftCornerOfText = (box[0, 0], box[0, 1]-5)
            cv2.putText(frame, text,bottomLeftCornerOfText,cv2.FONT_HERSHEY_DUPLEX,0.5,(0, 200, 200),1)
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        return frame


