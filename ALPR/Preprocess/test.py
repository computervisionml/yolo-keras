import numpy as np
import cv2
import imutils
from statistics import mode 
from PIL import Image
from preprocess_images import preprocess_image

#import numpy and
from skimage.filters import threshold_local


import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='parse the image locations')
    parser.add_argument('-src',
                        metavar='s',
                        type=str,
                        help='the path to source of the image')
    parser.add_argument('-dst',
                        metavar='d',
                        type=str,
                        help='the path to source of the image')
    args = parser.parse_args()
    image_source = args.src
    image = cv2.imread(image_source)
    output_image = preprocess_image(image)
    output_image.save(args.dst)
    
