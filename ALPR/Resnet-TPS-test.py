from PIL import Image
from OCR.OCR.ocr import ResNetSeqAttn
image = Image.open("./samples/hr.png").convert('L')
obj = ResNetSeqAttn(saved_model="best_norm_ED.pth")
result = obj.fetch_text([image, image, image])
print(result)