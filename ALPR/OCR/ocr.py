import pytesseract
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import torch
import  torch.nn.functional as F
from torch.serialization import save

from .modules.transformation import TPS_SpatialTransformerNetwork
from .modules.feature_extraction import VGG_FeatureExtractor, RCNN_FeatureExtractor, ResNet_FeatureExtractor
from .modules.sequence_modeling import BidirectionalLSTM
from .modules.prediction import Attention

from .utils import  AttnLabelConverter
from .dataset import AlignCollate

def tesseract(  image,
                config  = '--psm 7 -c tessedit_char_whitelist=0123456789ZXCVBNMLKJHGFDSAPOIUYTREWQ.%',
                lang    = 'eng'):
    '''Returns a string corresponding to the text in the image
    Args:
        image : PIL image 3 channel
    Returns
        A string corresponding to the text in the image
    '''
    image = image.convert('L')
    return pytesseract.image_to_string(image, config=config, lang=lang).upper()

class Model(torch.nn.Module):

    def __init__(self, num_class, imgH=32,imgW=100, num_fiducial=20, input_channel=1, output_channel=512,\
        Transformation='TPS', SequenceModeling='BiLSTM', Prediction='Attn', FeatureExtraction='ResNet',\
        batch_max_length=25,hidden_size=256):
        super(Model, self).__init__()
        
        self.stages = {'Trans': Transformation, 'Feat': FeatureExtraction,
                       'Seq': SequenceModeling, 'Pred': Prediction}

        """ Transformation """
        self.Transformation = TPS_SpatialTransformerNetwork(F=num_fiducial, I_size=(imgH, imgW),\
            I_r_size=(imgH, imgW), I_channel_num=input_channel)
        self.batch_max_length = batch_max_length
        self.FeatureExtraction = ResNet_FeatureExtractor(input_channel, output_channel)
        self.FeatureExtraction_output = output_channel  # int(imgH/16-1) * 512
        self.AdaptiveAvgPool = torch.nn.AdaptiveAvgPool2d((None, 1))  # Transform final (imgH/16-1) -> 1

        """ Sequence modeling"""
        if SequenceModeling == 'BiLSTM':
            self.SequenceModeling = torch.nn.Sequential(
                BidirectionalLSTM(self.FeatureExtraction_output, hidden_size, hidden_size),
                BidirectionalLSTM(hidden_size, hidden_size, hidden_size))
            self.SequenceModeling_output = hidden_size
        else:
            print('No SequenceModeling module specified')
            self.SequenceModeling_output = self.FeatureExtraction_output

        """ Prediction """
        if Prediction == 'CTC':
            self.Prediction = torch.nn.Linear(self.SequenceModeling_output, num_class)
        elif Prediction == 'Attn':
            self.Prediction = Attention(self.SequenceModeling_output, hidden_size, num_class)
        else:
            raise Exception('Prediction is neither CTC or Attn')

    def forward(self, input, text, is_train=True):
        """ Transformation stage """
        if not self.stages['Trans'] == "None":
            input = self.Transformation(input)

        """ Feature extraction stage """
        visual_feature = self.FeatureExtraction(input)
        visual_feature = self.AdaptiveAvgPool(visual_feature.permute(0, 3, 1, 2))  # [b, c, h, w] -> [b, w, c, h]
        visual_feature = visual_feature.squeeze(3)

        """ Sequence modeling stage """
        if self.stages['Seq'] == 'BiLSTM':
            contextual_feature = self.SequenceModeling(visual_feature)
        else:
            contextual_feature = visual_feature  # for convenience. this is NOT contextually modeled by BiLSTM

        """ Prediction stage """
        if self.stages['Pred'] == 'CTC':
            prediction = self.Prediction(contextual_feature.contiguous())
        else:
            prediction = self.Prediction(contextual_feature.contiguous(), text, is_train, batch_max_length=self.batch_max_length)

        return prediction

class ResNetSeqAttn:
    """ 
    CRAFT class to detect and straighten number plate crops from a given image
    """

    def __init__(self, saved_model, rgb=False, PAD = True, cuda = None, imgH=32, imgW=100, num_fiducial=20, input_channel=1, output_channel=512,\
        Transformation='TPS', SequenceModeling='BiLSTM', Prediction='Attn', FeatureExtraction='ResNet',\
        batch_max_length=25,hidden_size=256):
        if cuda is not None:
            if cuda:
                self.compute = 'cuda'
            else:
                self.compute = 'cpu'
        else:
            self.compute = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.saved_model = saved_model
        self.device = torch.device(self.compute)
        self.batch_max_length = batch_max_length
        self.Prediction = Prediction

        self.converter = AttnLabelConverter('0123456789abcdefghijklmnopqrstuvwxyz')
        self.num_class = len(self.converter.character)

        self.rgb = rgb
        self.SequenceModeling = SequenceModeling
        self.hidden_size = hidden_size
        self.Transformation = Transformation
        self.num_fiducial = num_fiducial
        if self.rgb:
            self.input_channel = 3
        else:
            self.input_channel = input_channel
        self.output_channel = output_channel
        self.imgH = imgH
        self.imgW = imgW
        self.FeatureExtraction = FeatureExtraction
        self.batch_max_length = batch_max_length
        self.hidden_size = hidden_size

        # load model
        assert saved_model is not None
        print('loading pretrained model from %s' % self.saved_model)
        self.model = Model(self.num_class, imgH=self.imgH, imgW=self.imgW, num_fiducial=self.num_fiducial,\
            input_channel=self.input_channel, output_channel=self.output_channel,\
            Transformation=self.Transformation, SequenceModeling=self.SequenceModeling, Prediction=self.Prediction, FeatureExtraction=self.FeatureExtraction,\
            batch_max_length=self.batch_max_length, hidden_size=self.hidden_size)
        self.model = torch.nn.DataParallel(self.model).to(self.device)
        self.model.load_state_dict(torch.load(self.saved_model, map_location=self.device))
        self.model.eval()

        # prepare data. two demo images from https://github.com/bgshih/crnn#run-demo
        self.AlignCollate_demo = AlignCollate(imgH=imgH, imgW=imgW, keep_ratio_with_pad=PAD)
        
    def fetch_text(self, images):
        '''
        Returns a list of list strings of text for each image in a given list
        Args:
            images [List of PIL image]: a list of PIL image in greyscale format
        Returns
            A list of list of strings
        '''
        image_tensors= self.AlignCollate_demo(images)
        results = []
        with torch.no_grad():
            for image_tensor in image_tensors:
                batch_size = image_tensor.size(0)
                image = image_tensor.to(self.device)
                # For max length prediction
                length_for_pred = torch.IntTensor([self.batch_max_length] * batch_size).to(self.device)
                text_for_pred = torch.LongTensor(batch_size, self.batch_max_length + 1).fill_(0).to(self.device)
                preds = self.model(torch.unsqueeze(image,0), text_for_pred, is_train=False)

                # select max probabilty (greedy decoding) then decode index to character
                _, preds_index = preds.max(2)
                preds_str = self.converter.decode(preds_index, length_for_pred)

                preds_prob = F.softmax(preds, dim=2)
                preds_max_prob, _ = preds_prob.max(dim=2)
                for pred, pred_max_prob in zip( preds_str, preds_max_prob):
                    if 'Attn' in self.Prediction:
                        pred_EOS = pred.find('[s]')
                        pred = pred[:pred_EOS]  # prune after "end of sentence" token ([s])
                        pred_max_prob = pred_max_prob[:pred_EOS]
                        results.append(pred)
            return results


