from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from OCR.ocr import ANPR

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse

def show_roi(road_name):
    polygons = get_roi(road_name)
    pts = np.array(polygons, np.int32)
    pts = pts.reshape((-1,1,2))
    return pts, polygons

def get_roi(road_name):
    with open('./ROI/line_config.json', 'r+') as f:
        data = json.load(f)
        polygons = data[road_name]['polygon']
        return polygons


if __name__ == '__main__': 
  
  # initialise objects
  detect_vehicles = DetectVehicles()
  alpr = ANPR()
  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-video_path',
                      metavar='s',
                      type=str,
                      default='./samples/video1.mp4',
                      help='the rtsp video source')
  
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')
  
  args = parser.parse_args()
  video_path = args.video_path
  camera_id = args.camera_id
  vid = cv2.VideoCapture(video_path)
  width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
  assert (width==int(vid.get(3))), "Not matching - width"
  height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
  assert (height==int(vid.get(4))), "Not matching - height"
  fps = int(vid.get(cv2.CAP_PROP_FPS))
  codec = cv2.VideoWriter_fourcc(*'XVID')
  
  frame_skip = 0
  # checking frames on video
  while vid.isOpened():
    frame_skip += 1
    if frame_skip % 5 != 0:
      continue
    ret , original_frame = vid.read()
    if not ret:
      continue
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break  
      
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    bboxes = detect_vehicles.get_bbox(image_bytes)
    for bbox in bboxes:
        frame = original_frame[bbox[1]:bbox[3],bbox[0]:bbox[2]]
        h, w = frame.shape[:2]
        frame = alpr.recognition(frame)
        frame = cv2.resize(frame,(w,h))
        original_frame[bbox[1]:bbox[3],bbox[0]:bbox[2]] = frame
        
    cv2.resize(original_frame,(800, 800))
    cv2.imshow("Frame",original_frame)
    cv2.waitKey(1)