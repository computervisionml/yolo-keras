from Detect.CRAFT_pytorch.Detect import CRAFT
from Preprocess import preprocess_images
from OCR.ocr import tesseract, ResNetSeqAttn

import numpy as np
import cv2
from PIL import Image

import torch

import os
import re

class LicensePlate:
    '''
    class to detect and straighten number plate crops from a given image and to 
    decipher the number on it.
    '''

    def __init__(self, cuda = None):
        if cuda is None:
            self.CUDA_AVAILABLE = torch.cuda.is_available()
        else:
            self.CUDA_AVAILABLE = cuda
        
        self.text_detector = CRAFT(self.CUDA_AVAILABLE)

    def string_checks(self, detect_text):
        '''
        Do simple checks on the image and return the corrected string
        Args:
            detected_string (str): a string corresponding to text detected by the text decipher
        Return
            a string or an empty string if no rectifications are available.
        '''
        detected_text = re.sub('[^a-zA-Z0-9]','',detect_text.upper())
        num_digits = sum(list(map(lambda x:1 if x.isdigit() else 0,set(detected_text))))
        if num_digits >=2:
            return detect_text
        else:
            return ""

    def fetch_plate_number(self, image):
        '''
        Returns an empty string or a string corresponding to the license plate detected in the image
        Args:
            image (np.ndarray): cv2 image of a vehicle whose number plate is to be fetched
        Returns:
            a string corresponding to the license plate number or an empty string in case of failure.
        '''
        number_plate_strings = []
        bounding_boxes = self.text_detector.detect_text(image)
        for part_img, box in self.text_detector.yield_image_crops(image, bounding_boxes):
            preprocessed_image = preprocess_images.preprocess_image(part_img)
            detect_text = tesseract(preprocessed_image)
            detect_text = self.string_checks(detect_text)
            if detect_text != "":
                number_plate_strings.append(tesseract(preprocessed_image))
        return number_plate_strings

class LicensePlate_V2:
    '''
    class to detect and straighten number plate crops from a given image and to 
    decipher the number on it.
    '''

    def __init__(self, saved_model,cuda = None):
        if cuda is None:
            self.CUDA_AVAILABLE = torch.cuda.is_available()
        else:
            self.CUDA_AVAILABLE = cuda
        
        self.text_localizer = CRAFT(self.CUDA_AVAILABLE)
        self.ocr = ResNetSeqAttn(saved_model=saved_model)

    def string_checks(self, detect_text):
        '''
        Do simple checks on the image and return the corrected string
        Args:
            detected_string (str): a string corresponding to text detected by the text decipher
        Return
            a string or an empty string if no rectifications are available.
        '''
        detected_text = re.sub('[^a-zA-Z0-9]','',detect_text.upper())
        num_digits = sum(list(map(lambda x:1 if x.isdigit() else 0,set(detected_text))))
        if num_digits >=2:
            return detect_text
        else:
            return ""

    def fetch_plate_number(self, image):
        '''
        Returns an empty string or a string corresponding to the license plate detected in the image
        Args:
            image (np.ndarray): cv2 image of a vehicle whose number plate is to be fetched
        Returns:
            a string corresponding to the license plate number or an empty string in case of failure.
        '''
        number_plate_strings = []
        bounding_boxes = self.text_localizer.detect_text(image)
        for part_img, box in self.text_localizer.yield_image_crops(image, bounding_boxes):
            preprocessed_image = preprocess_images.preprocess_image(part_img)
            detect_text_list = self.ocr.fetch_text([preprocessed_image.convert('L')])
            detect_text_list = [self.string_checks(text) for text in detect_text_list]
        return detect_text_list