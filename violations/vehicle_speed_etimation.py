# imports 
from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from tracking.vehicle_tracking import TrackVehicles


from collections import deque
import json
import time
import math
import datetime
import argparse

class SpeedEstimation:
    def __init__(self):
        self.car_location1 = dict()
        self.car_location2 = dict()
        
    def estimateSpeed(self, location1, location2, fps):
        # we get the values of location 1 and 2
        # we calculate the difference in pixels in a frame through distance formula
        d_pixels = math.sqrt(math.pow(location2[0] - location1[0], 2) + math.pow(location2[1] - location1[1], 2))
        
        # ppm = location2[2] / carWidht
        # we calculate distance per frame in meters by dividing pixels by pixel per meter
        ppm = 8.8
        # ppm = 14.28
        d_meters = d_pixels / ppm
        # print("d_pixels=" + str(d_pixels), "d_meters=" + str(d_meters))
        # fps = 18
        # we determine the speed by distance for a frame with frame per second and to convert it to kmph we multiply 3.6
        speed = d_meters * fps * 3.6
        return speed

    def get_speed(self, track_vehicles, frame_counter, fps):
        tracked_bboxes = []
        speed = None
        for track in track_vehicles.tracked_vehicles():
            
            bbox = track.to_tlbr() # Get the corrected/predicted bounding box
            class_name = track.get_class() #Get the class name of particular object
            tracking_id = track.track_id # Get the ID for the particular track
            index = track_vehicles.key_list[track_vehicles.val_list.index(class_name)] # Get predicted object index by object name
            
            if (frame_counter) % 10 != 0:
                self.car_location1[track.track_id] = track.to_tlbr()
            else:
                self.car_location2[track.track_id] = track.to_tlbr()
                
            x1, y1, x2, y2 = track.to_tlbr()
            w1 = x2 - x1
            if track.track_id in self.car_location1 and track.track_id in self.car_location2:
                [x1, y1, x1_bar, y1_bar] = self.car_location1[track.track_id]
                [x2, y2, x2_bar, y2_bar] = self.car_location2[track.track_id]
                w1 = x1_bar - x1
                if [x1, y1, x1_bar, y1_bar] != [x2, y2, x2_bar, y2_bar]:
                    if (speed[track.track_id] == None or speed[track.track_id] == 0):
                            speed[track.track_id] = estimateSpeed(self.self.car_location1[track.track_id] , self.car_location2[track.track_id])

                    if speed[track.track_id] != None:
                            speed = str(int(speed[track.track_id])) + " km/hr"
            
            speed = "" if speed is None else speed           
            tracked_bboxes.append(bbox.tolist() + [tracking_id, index] + [speed])
            
            return tracked_bboxes

