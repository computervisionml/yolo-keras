from PIL import Image
import cv2

import numpy as np

import datetime
from collections import deque
import json
import time
import math
import logging
import sys

class TrafficViolation:
  def __init__(self):
    self.color = {"green":(0,255,0),"red":(0,0,255)}
    self.logging_format = "\n%(camera_id)s : %(class_name)s-%(track_id)s %(message)s %(time)s\n"
    logging.basicConfig(handlers=[logging.FileHandler("tracking/violations.log"),logging.StreamHandler(sys.stdout)], format=self.logging_format, level=logging.INFO)
    self.logger = logging.getLogger("violations_logger")  
    self.already_tracked = [] 

  def ccw(self, A, B, C):
    return (C[1] - A[1]) * (B[0] - A[0]) > (B[1] - A[1]) * (C[0] - A[0])

  def intersect(self, A, B, C, D):
    return self.ccw(A, C, D) != self.ccw(B, C, D) and self.ccw(A, B, C) != self.ccw(A, B, D)

  def vector_angle(self, midpoint, previous_midpoint):
    x = midpoint[0] - previous_midpoint[0]
    y = midpoint[1] - previous_midpoint[1]
    return math.degrees(math.atan2(y, x))

  def check_violation(self, original_frame, track_vehicles, roi_line, midpoint, previous_midpoint, track_id, signal_status):
    intersect_info = []
    origin_midpoint = (midpoint[0], original_frame.shape[0] - midpoint[1])
    origin_previous_midpoint = (previous_midpoint[0], original_frame.shape[0] - previous_midpoint[1])
    if signal_status == "red" and self.intersect(midpoint, previous_midpoint, roi_line[0], roi_line[1]) and track_id not in track_vehicles.already_counted:
      track_vehicles.already_counted.append(track_id)  # Set already counted for ID to true.
      angle = self.vector_angle(origin_midpoint, origin_previous_midpoint)
      intersection_time = datetime.datetime.now() - datetime.timedelta(microseconds=datetime.datetime.now().microsecond)
      intersect_info.extend([track_id, intersection_time])
      if abs(angle) > 0:
        return intersect_info
  
  def raise_alert_1(self, violation_info):
    self.logger.info('Red Light traffic violation at: ', extra=violation_info)
  
  def raise_alert_2(self, violation_info):
    self.logger.info('Wrong way violation: ', extra=violation_info)
  
  def track_violations(self, original_frame, camera_id, track_vehicles, roi_line, signal_status):
    tracked_bboxes = []
    direction = None
    for track in track_vehicles.tracked_vehicles(): 
      if not track.is_confirmed() or track.time_since_update > 5:
        continue 
      
      bbox = track.to_tlbr() # Get the corrected/predicted bounding box
      class_name = track.get_class() #Get the class name of particular object
      tracking_id = track.track_id # Get the ID for the particular track
      index = track_vehicles.key_list[track_vehicles.val_list.index(class_name)] # Get predicted object index by object name

      midpoint = track.tlbr_midpoint(bbox)
      origin_midpoint = (midpoint[0], original_frame.shape[0] - midpoint[1])  # get midpoint respective to botton-left
      if track.track_id not in track_vehicles.memory:
        track_vehicles.memory[track.track_id] = deque(maxlen=5)
      
      track_vehicles.memory[track.track_id].append(midpoint)
      
      if len(track_vehicles.memory[track.track_id]) > 2:
        previous_midpoint = track_vehicles.memory[track.track_id][-2]
      else:
        previous_midpoint = track_vehicles.memory[track.track_id][0]
      origin_previous_midpoint = (previous_midpoint[0], original_frame.shape[0] - previous_midpoint[1])
      
      if len(track_vehicles.memory[track.track_id]) == 5:
        last_midpoint = track_vehicles.memory[track.track_id][0]
        origin_last_midpoint = (last_midpoint[0], original_frame.shape[0] - last_midpoint[1])
      
        dX = last_midpoint[0] - midpoint[0] #pts[-10][0] - pts[i][0]
        dY = last_midpoint[1] - midpoint[1] #pts[-10][1] - pts[i][1]
        (dirX, dirY) = ("", "")
        if np.abs(dX) > 5:
          dirX = "West" if np.sign(dX) == 1 else "East"
        if np.abs(dY) > 5:
          dirY = "North" if np.sign(dY) == 1 else "South"
        if dirX != "" and dirY != "":
          direction = "{}-{}".format(dirY, dirX)
        else:
          direction = dirX if dirX != "" else dirY
      
      direction = "" if direction is None else direction
      tracked_bboxes.append(bbox.tolist() + [tracking_id, index] + [direction])

      violation_vehicles = self.check_violation(original_frame, track_vehicles, roi_line, midpoint, previous_midpoint, tracking_id, signal_status)
      
      if violation_vehicles != None:
          violation_info = {"camera_id":camera_id, "class_name":class_name, "track_id":violation_vehicles[0], "time":violation_vehicles[1].strftime("%d-%m-%Y %H:%M:%S")}
          self.raise_alert_1(violation_info)
          
      if ("South" not in direction) and ("East" in direction or "West" in direction):
          if tracking_id not in self.already_tracked:
            violation_time = datetime.datetime.now() - datetime.timedelta(microseconds=datetime.datetime.now().microsecond)
            violation_info = {"camera_id":camera_id, "class_name":class_name, "track_id": tracking_id, "time":violation_time.strftime("%d-%m-%Y %H:%M:%S")}
            self.raise_alert_2(violation_info)
            self.already_tracked.append(tracking_id)
        
       # Structure data, that we could use it with our draw_bbox function
    return tracked_bboxes
    
    

