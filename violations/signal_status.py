import json
from flask import Flask, request, jsonify
app = Flask(__name__)


@app.route('/signal_status', methods=['GET', 'POST'])
def get_signal_status():
    global signal_color
    json_signal_status = request.get_json()
    with open("signal_status_main.json","w") as json_file:
      json.dump(json_signal_status,json_file,indent=6)    
    return json.dumps({'success':True}) 


if __name__ == '__main__':
    app.run(host= '0.0.0.0',debug=True)