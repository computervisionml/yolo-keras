from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from tracking.vehicle_tracking import TrackVehicles
from violations.vehicle_speed_etimation import SpeedEstimation

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse

if __name__ == '__main__': 
  
  # initialise objects
  detect_vehicles = DetectVehicles()
  track_vehicles = TrackVehicles()
  track_speed = SpeedEstimation()
  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-video_path',
                      metavar='s',
                      type=str,
                      default='samples/hubli_cam.mp4',
                      help='the rtsp video source')
  
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')
  
  args = parser.parse_args()
  video_path = args.video_path
  camera_id = args.camera_id
  vid = cv2.VideoCapture(video_path)
  width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
  assert (width==int(vid.get(3))), "Not matching - width"
  height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
  assert (height==int(vid.get(4))), "Not matching - height"
  fps = int(vid.get(cv2.CAP_PROP_FPS))
  codec = cv2.VideoWriter_fourcc(*'XVID')
  output_path = "detect-violation.avi"
  # out = cv2.VideoWriter(output_path, codec, fps, (width,height))
  
  frame_counter = 0
  # checking frames on video
  while vid.isOpened():
    frame_counter += 1
    if frame_counter % 5 != 0:
      continue
    ret , original_frame = vid.read()
    if not ret:
      continue
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break  
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    bboxes = detect_vehicles.get_bbox(image_bytes)
    # tracking
    detections = track_vehicles.format_detection_bbox(original_frame, bboxes)
    track_vehicles.update_tracker(detections)

    # use traffic_violations object
    tracked_bboxes = track_speed.get_speed(track_vehicles, frame_counter, fps)
    track_vehicles.clear_memory()
    updated_bboxes = track_vehicles.bbox_nms_format(tracked_bboxes)
    bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_vehicles.non_max_suppression_fast(updated_bboxes)
    original_frame = utils_image.draw_track_bbox(original_frame, bboxes, track_vehicles.YOLO_Classes, tracking=True)
    cv2.imshow("Frame",original_frame)
    cv2.waitKey(1)