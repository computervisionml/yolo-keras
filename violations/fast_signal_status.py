from fastapi import FastAPI
import json

app = FastAPI()

@app.get("/signal_status/{signal_status}")
async def get_signal_status(signal_status: str):
    json_signal_status = {"color": signal_status}
    with open("signal_status.json","w") as json_file:
        json_file.seek(0)
        json_file.truncate()
        json.dump(jsonify(json_signal_status),json_file,indent=6)    
    return json_signal_status