# imports 
from PIL import Image
import cv2

import numpy as np

from matplotlib import path

from math import *
from collections import deque
import json
import time
import math
import datetime
import argparse

# main function
class VehicleParking:
    def __init__(self): 
        
        self.vehicle_location1 = dict()
        self.vehicle_location2 = dict()
        self.vehicle_in_no_parking = list()
        self.v_np_tracker = dict()
    
    def show_roi(self, road_name):
        polygons = self.get_roi(road_name)
        pts = np.array(polygons, np.int32)
        pts = pts.reshape((-1,1,2))
        return pts, polygons

    def get_roi(self, road_name):
        with open('./roi_config.json', 'r+') as f:
            data = json.load(f)
            polygons = data[road_name]['polygon']
            return polygons

    # get distance between two centroids
    def great_circle_distance(self, coordinates1, coordinates2):
        latitude1, longitude1 = coordinates1
        print(latitude1)
        print(longitude1)
        latitude2, longitude2 = coordinates2
        print(latitude2)
        print(longitude2)
        d = pi / 180  # factor to convert degrees to radians
        return acos(int(sin(longitude1*d) * sin(longitude2*d) +
                    cos(longitude1*d) * cos(longitude2*d) *
                    cos((latitude1 - latitude2) * d))) / d

    # check if two centroids are in the range
    def in_range(self, coordinates1, coordinates2, range1):
        k = self.great_circle_distance(coordinates1, coordinates2)
        print(k)
        return int(k) <= range1
    
    def get_path(self):
        pts, polygons = self.show_roi("hubli_cam_1")
        polygon_xy1 = (int(polygons[0][0]), int(polygons[0][1]))
        polygon_xy2 = (int(polygons[1][0]), int(polygons[1][1]))
        polygon_xy1_bar = (int(polygons[2][0]), int(polygons[2][1]))
        polygon_xy2_bar = (int(polygons[3][0]), int(polygons[3][1]))
        p = path.Path([polygon_xy1, polygon_xy2, polygon_xy1_bar, polygon_xy2_bar])
        return p
        
    def parking_violation(self, frame_counter, track_vehicles):
        
        p = self.get_path()
        tracked_bboxes = []
        parking_status = None
        
        for track in track_vehicles.tracked_vehicles():
            if track.track_id in self.v_np_tracker:
                self.v_np_tracker[track.track_id]['current_time'] = datetime.datetime.now()
            else:
                self.v_np_tracker[track.track_id] = dict()
                self.v_np_tracker[track.track_id]['start_time'] = datetime.datetime.now()
                self.v_np_tracker[track.track_id]["counter"] = 0
            if (frame_counter) % 20 == 0:
                [x1, y1, x1_bar, y1_bar] = track.to_tlbr()
                x, y = int((x1+x1_bar)/2), int((y1+y1_bar)/2)
                self.vehicle_location1[track.track_id] = [x,y]
            else:
                [x2, y2, x2_bar, y2_bar] = track.to_tlbr()
                x, y = int((x2+x2_bar)/2), int((y2+y2_bar)/2)
                self.vehicle_location2[track.track_id] = [x,y]
            if track.track_id in self.vehicle_location1 and track.track_id in self.vehicle_location2:
                [xx1, yy1] = self.vehicle_location1[track.track_id]
                [xx2, yy2] = self.vehicle_location2[track.track_id]
                
                if self.in_range([xx1, yy1], [xx2, yy2], 20):
                    if not p.contains_points([(xx2, yy2)])[0]: #important
                        print("inside no parking zone")
                        continue
                    else:
                        if track.track_id in self.v_np_tracker:
                            self.v_np_tracker[track.track_id]["counter"] += 1
                        if self.v_np_tracker[track.track_id]["counter"] > 10:
                            self.vehicle_in_no_parking.append(track.track_id)
                            
            if track.track_id in self.v_np_tracker:
                if "current_time" in  self.v_np_tracker[track.track_id]:
                    tot_time = self.v_np_tracker[track.track_id]['current_time'] - self.v_np_tracker[track.track_id]['start_time']
                    tot_time = tot_time.seconds
                    if (track.track_id in self.vehicle_in_no_parking) and  tot_time>=30:            
                        parking_status = "NP"
            
            
            bbox = track.to_tlbr() # Get the corrected/predicted bounding box
            class_name = track.get_class() #Get the class name of particular object
            tracking_id = track.track_id # Get the ID for the particular track
            index = track_vehicles.key_list[track_vehicles.val_list.index(class_name)] # Get predicted object index by object name
            parking_status = "" if parking_status is None else parking_status      
                 
            tracked_bboxes.append(bbox.tolist() + [tracking_id, index] + [parking_status])
            
            return tracked_bboxes
                