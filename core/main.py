from PIL import Image
import cv2
import numpy as np

from vechicle_detection import DetectVehicles
from vechicle_tracking import TrackVehicles
from vehicle_violation import TrafficViolation
import core.utils as utils
import CONFIG

from collections import deque
import json
import time
import math
import datetime
import argparse

if __name__ == '__main__': 
  # initialise objects
  detect_vehicles = DetectVehicles()
  track_vehicles = TrackVehicles()
  track_violation = TrafficViolation()  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-video_path',
                      metavar='s',
                      type=str,
                      default='rtsp://admin:admin123@112.133.197.90:4554/cam/realmonitor?channel=1&subtype=1',
                      help='the rtsp video source')
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='4554',
                    help='the rtsp video source')
  
  args = parser.parse_args()
  video_path = args.video_path
  camera_id = args.camera_id
  vid = cv2.VideoCapture(video_path)
  width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
  assert (width==int(vid.get(3))), "Not matching - width"
  height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
  assert (height==int(vid.get(4))), "Not matching - height"
  roi_line = [(int(0*width), int(0.55 * height)), (int(0.8 * width), int(0.25 * height))]
  frame_skip = 0
  # checking frames on video
  while vid.isOpened():
    frame_skip += 1
    if frame_skip % 5 != 0:
      continue
    ret , original_frame = vid.read()
    if not ret:
      continue
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break  
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    with open("signal_status.json","r") as json_file:
      signal_json = json.load(json_file)  
    signal_status = signal_json['color']
    original_frame, bboxes = detect_vehicles.get_bbox(image_bytes)
    # tracking
    detections = track_vehicles.format_detection_bbox(original_frame, bboxes)
    track_vehicles.update_tracker(detections)
    if track_violation.track_violations(original_frame, camera_id, track_vehicles, roi_line, signal_status) is None:
      continue
    # use traffic_violations object
    tracked_bboxes = track_violation.track_violations(original_frame, camera_id, track_vehicles, roi_line, signal_status)
    track_vehicles.clear_memory()
    updated_bboxes = track_violation.bbox_nms_format(tracked_bboxes)
    bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_violation.non_max_suppression_fast(updated_bboxes)
    original_frame = utils.draw_track_bbox(original_frame, bboxes, tracking=True)
    cv2.line(original_frame, roi_line[0], roi_line[1], track_violation.color[signal_status], 2)
    original_frame = cv2.cvtColor(original_frame, cv2.COLOR_BGR2RGB) 
    cv2.imshow("Frame",original_frame)
    cv2.waitKey(1)