import tensorflow as tf

from PIL import Image
import cv2
import numpy as np

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from tools import utils_image
from core import config
from core import eval
from core import models

import psutil
import time
import os
import multiprocessing
from collections import namedtuple
import argparse

configs = ConfigProto()
configs.gpu_options.allow_growth = True
session = InteractiveSession(config=configs)

physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    

class DetectVehicles:
    # Object Detection
    def __init__(self):
        self.class_file_path = config.classes_path
        self.anchors = config.anchors
        self.class_names = config.classes_names
        self.model_file_path = config.yolo_weights
        self.num_anchors = len(self.anchors)
        self.num_classes = len(self.class_names)
        self.YOLO_Classes = dict(enumerate(self.class_names))
        self.key_list = list(self.YOLO_Classes.keys()) 
        self.val_list = list(self.YOLO_Classes.values())
        self.class_mapping = dict(enumerate(self.class_names))
        self.class_mapping = {key : self.class_mapping[key] for key in self.class_mapping}
        self.colors = utils_image.get_random_colors(len(self.class_names))
        self.model = models.YOLO()()
        self.model.load_weights(self.model_file_path)

    def image_crops(self, frame, detections):
        for bbox in detections:
            yield frame[bbox[1]:bbox[3],bbox[0]:bbox[2]]
        
    
    def get_bbox(self, image_bytes):
        '''
        Perform object detection on image bytes
        
        Args:
        image_bytes (bytes): image in bytes
        
        Returns:
        detections (list): list of detections in left, top, right, bottom, score, class format
        '''
        cameraId = image_bytes["CameraId"]
        nparr = np.frombuffer(image_bytes["ImageBytes"], np.uint8)
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        new_image = utils_image.resize_image(image, config.image_input_shape)
        new_image = np.array(new_image, dtype='float32')
        new_image /= 255.
        new_image = np.expand_dims(new_image, 0)
        
        feats = self.model.predict(new_image)
        boxes, scores, classes = eval.yolo_eval(feats, self.anchors, len(self.class_names), (image.shape[0], image.shape[1]))
        detections = utils_image.bbox_details(image, boxes, scores, classes, self.class_names)
        
        # remove after testing
        # print("Classes: ",classes)
        # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        # image = utils_image.draw_rectangle(image, boxes, scores, classes, self.class_names, self.colors, mode='pillow')
        # cv2.imshow("Detection",image)
        # cv2.waitKey(1)
        
        return detections

    
    
