from detection.vehicle_detection import DetectVehicles

from imutils.video import FileVideoStream
from imutils.video import FPS
import imutils
import cv2

import time
import argparse

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model', type=str, help='input h5 model path', default=r"C:\Users\Dell\Desktop\computer_vision\yolo-tf2\samples\yolov4.h5")
    parser.add_argument('-i', '--video', type=str, help='input image file path', default=r'C:\Users\Dell\Desktop\computer_vision\yolo-tf2\samples\video2.MOV')
    parser.add_argument('-camera_id', metavar='s', type=str, default='5554', help='the rtsp video source')
    
    args = parser.parse_args()
    model_file_path = args.model
    video_file_path = args.video
    camera_id = args.camera_id
    
    fvs = FileVideoStream(video_file_path,queue_size=64).start()
    time.sleep(1.0)
    fps = FPS().start()
    
    detect_vehicles = DetectVehicles()

    while fvs.more():
        image = fvs.read()
        img_buffer = cv2.imencode('.jpg', image)[1].tobytes()
        image_bytes = {}
        image_bytes["ImageBytes"] = img_buffer
        image_bytes["ContentType"]= 'image/jpg'
        image_bytes["CameraId"]= camera_id
        bboxes = detect_vehicles.get_bbox(image_bytes)