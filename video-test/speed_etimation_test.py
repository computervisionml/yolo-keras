# imports 
from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from tracking.vehicle_tracking import TrackVehicles

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse


def estimateSpeed(location1, location2, fps):
    # we get the values of location 1 and 2
    # we calculate the difference in pixels in a frame through distance formula
    d_pixels = math.sqrt(math.pow(location2[0] - location1[0], 2) + math.pow(location2[1] - location1[1], 2))

    # ppm = location2[2] / carWidht
    # we calculate distance per frame in meters by dividing pixels by pixel per meter
    ppm = 8.8
    # ppm = 14.28
    d_meters = d_pixels / ppm
    #print("d_pixels=" + str(d_pixels), "d_meters=" + str(d_meters))
    fps = 12
    # we determine the speed by distance for a frame with frame per second and to convert it to kmph we multiply 3.6
    speed = d_meters * fps * 3.6
    return speed

# main function
if __name__ == '__main__': 
    # initialise objects
    detect_vehicles = DetectVehicles()
    track_only = ["car","truck","motorbike","bus","bicycle"]
    track_vehicles = TrackVehicles()
    speed = [None] * 1000
    
    # get the live feed from the server
    parser = argparse.ArgumentParser(description='parse the image locations')
    parser.add_argument('-video_path',
                    metavar='s',
                    type=str,
                    default='rtsp://admin:admin@123@112.133.197.90:2554/cam/realmonitor?channel=1&subtype=1',
                    help='the rtsp video source')

    parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')

    # parse the args
    args = parser.parse_args()
    video_path = args.video_path
    
    car_location1 = dict()
    car_location2 = dict()
    camera_id = args.camera_id
    
    # Capture the video frames
    vid = cv2.VideoCapture(video_path)
    # vid = cv2.VideoCapture('speed_test.mp4')

    # assign video dimensions
    width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
    print(width)
    assert (width==int(vid.get(3))), "Not matching - width"
    height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print(height)
    assert (height==int(vid.get(4))), "Not matching - height"
    fps = int(vid.get(cv2.CAP_PROP_FPS))
    print("FPS: ", fps)
    codec = cv2.VideoWriter_fourcc(*'XVID')
    output_path = "detect-speed.avi"
    out = cv2.VideoWriter(output_path, codec, fps, (width,height))
    frame_skip = 0
    # checking frames on video
    frame_counter = 0
    while vid.isOpened():
        frame_skip += 1
        # skip to every 10th frame
        if frame_skip % 10 != 0:
            frame_counter += 1
            continue
        # get the frame
        ret , original_frame = vid.read()
        if not ret:
            continue
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break  
        # change the frame to bytes
        img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
        image_bytes = {}
        image_bytes["ImageBytes"] = img_buffer
        image_bytes["ContentType"]= 'image/jpg'
        image_bytes["CameraId"]= camera_id
        
        
        # detect the bounding box and 
        bboxes = detect_vehicles.get_bbox(image_bytes)
        # track the vehicle using bounding boxes
        detections = track_vehicles.format_detection_bbox(original_frame, bboxes)
        track_vehicles.update_tracker(detections)
        # draw the track box and show the image

        tracked_bboxes = []
        vehicle_speed = None
        for track in track_vehicles.tracked_vehicles():
            # print("track id")
            # print(track.track_id)
            # print("frame counter")
            # print(frame_counter)
            
            bbox = track.to_tlbr() # Get the corrected/predicted bounding box
            class_name = track.get_class() #Get the class name of particular object
            tracking_id = track.track_id # Get the ID for the particular track
            index = track_vehicles.key_list[track_vehicles.val_list.index(class_name)] # Get predicted object index by object name
            
            if (frame_counter) % 10 != 0:
                car_location1[track.track_id] = track.to_tlbr()
                # print("car_location1")
            else:
                car_location2[track.track_id] = track.to_tlbr()
                # print("car_location2")
            x1, y1, x2, y2 = track.to_tlbr()
            w1 = x2 - x1
            if track.track_id in car_location1 and track.track_id in car_location2:
                [x1, y1, x1_bar, y1_bar] = car_location1[track.track_id]
                [x2, y2, x2_bar, y2_bar] = car_location2[track.track_id]
                w1 = x1_bar - x1
                if [x1, y1, x1_bar, y1_bar] != [x2, y2, x2_bar, y2_bar]:
                    if (speed[track.track_id] == None or speed[track.track_id] == 0):
                            speed[track.track_id] = estimateSpeed(car_location1[track.track_id] , car_location2[track.track_id], fps)

                    if speed[track.track_id] != None:
                            # cv2.putText(original_frame, str(int(speed[track.track_id])) + " km/hr", (int(x1 + w1/2), int(y1-5)),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 0), 2)
                            vehicle_speed = str(int(speed[track.track_id])) + " km/hr"
                            
            vehicle_speed = "" if vehicle_speed is None else vehicle_speed
            tracked_bboxes.append(bbox.tolist() + [tracking_id, index] + [vehicle_speed])

        updated_bboxes = track_vehicles.bbox_nms_format(tracked_bboxes)
        bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_vehicles.non_max_suppression_fast(updated_bboxes)
        original_frame = utils_image.draw_track_bbox(original_frame, bboxes, track_vehicles.YOLO_Classes, tracking=True)
        
        # print(car_location1)
        # print(car_location2)
        
    
        track_vehicles.clear_memory()
        out.write(original_frame)
        cv2.imshow("Frame",original_frame)
        cv2.waitKey(1)