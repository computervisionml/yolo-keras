from tools import utils_image
from core import config
from core import eval
from core import models

import tensorflow as tf

import cv2

import numpy as np

import argparse
import time

from imutils.video import FileVideoStream
from imutils.video import FPS
import imutils


devices = tf.config.experimental.list_physical_devices('GPU')
if devices:
    tf.config.experimental.set_memory_growth(devices[0], True)

parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', type=str, help='input h5 model path', default="../weights/yolov4.h5")
parser.add_argument('-i', '--video', type=str, help='input image file path', default='../samples/video2.MOV')


args = parser.parse_args()
model_file_path = args.model
video_file_path = args.video


class_file_path = config.classes_path
anchors = config.anchors
class_names = config.classes_names

num_anchors = len(anchors)
num_classes = len(class_names)
class_mapping = dict(enumerate(class_names))
class_mapping = {class_mapping[key]: key for key in class_mapping}

colors = utils_image.get_random_colors(len(class_names))
model = models.YOLO()()
model.load_weights(model_file_path)

fvs = FileVideoStream(video_file_path,queue_size=16).start()
time.sleep(1.0)
fps = FPS().start()

while fvs.more():

    image = fvs.read()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    new_image = utils_image.resize_image(image, config.image_input_shape)

    new_image = np.array(new_image, dtype='float32')
    new_image /= 255.
    new_image = np.expand_dims(new_image, 0)
    feats = model.predict(new_image)

    boxes, scores, classes = eval.yolo_eval(feats, anchors, len(class_names), (image.shape[0], image.shape[1]))
    out_boxes, out_scores, out_classes = boxes[:5], scores[:5], classes[:5]

    image = utils_image.draw_rectangle(image, boxes, scores, classes, class_names, colors, mode='pillow')
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    cv2.imshow('img', image)
    cv2.waitKey(1)
    fps.update()

fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

cv2.destroyAllWindows()
fvs.stop()
