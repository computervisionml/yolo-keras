from tools import utils_image
from core import config
from core import eval
from core import models

import cv2
import numpy as np


import argparse

import tensorflow as tf

devices = tf.config.experimental.list_physical_devices('GPU')
if devices:
    tf.config.experimental.set_memory_growth(devices[0], True)


parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', type=str, help='input h5 model path', default="weights/yolov4.h5")
parser.add_argument('-i', '--image', type=str, help='input image file path', default='samples/000030.jpg')


args = parser.parse_args()
model_file_path = args.model
image_file_path = args.image


class_file_path = config.classes_path
anchors = config.anchors
class_names = config.classes_names

num_anchors = len(anchors)
num_classes = len(class_names)
class_mapping = dict(enumerate(class_names))
class_mapping = {class_mapping[key]: key for key in class_mapping}

colors = utils_image.get_random_colors(len(class_names))
model = models.YOLO()()
model.load_weights(model_file_path)



image = cv2.imread(image_file_path)
print(image.shape)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


new_image = utils_image.resize_image(image, config.image_input_shape)
print("resized image: ",new_image.shape)

new_image = np.array(new_image, dtype='float32')
new_image /= 255.
new_image = np.expand_dims(new_image, 0)
feats = model.predict(new_image)


boxes, scores, classes = eval.yolo_eval(feats, anchors, len(class_names), (image.shape[0], image.shape[1]))
out_boxes, out_scores, out_classes = boxes[:5], scores[:5], classes[:5]


image = utils_image.draw_rectangle(image, boxes, scores, classes, class_names, colors, mode='pillow')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
cv2.namedWindow("img", cv2.WINDOW_NORMAL)
cv2.imshow('img', image)
cv2.waitKey()
