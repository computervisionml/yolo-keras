import tensorflow as tf

from PIL import Image
import cv2

import numpy as np

import psutil
import time
import os
import multiprocessing
from collections import deque
import math 

from core import config

from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from deep_sort import generate_detections as gdet

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

configs = ConfigProto()
configs.gpu_options.allow_growth = True
session = InteractiveSession(config=configs)

physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

class TrackVehicles:
    def __init__(self):
      self.class_file_path = config.classes_path
      self.class_names = config.classes_names
      self.YOLO_Classes = dict(enumerate(self.class_names))
      self.num_classes = len(self.class_names)
      self.key_list = list(self.YOLO_Classes.keys()) 
      self.val_list = list(self.YOLO_Classes.values())
      self.deepSORT_model = config.ds_weights
      self.encoder = gdet.create_box_encoder(self.deepSORT_model, batch_size=1)
      self.max_cosine_distance = 0.7
      self.nn_budget = None
      self.metric = nn_matching.NearestNeighborDistanceMetric("cosine", self.max_cosine_distance, self.nn_budget)
      self.tracker = Tracker(self.metric)
      self.track_only = ["car","truck","motorbike","bus","bicycle"]
      self.memory = {}
      self.already_counted = deque(maxlen=50)
    
    def non_max_suppression_fast(self, boxes, iou_threshold=0.33):
      '''
      NMS
      
      Args:
      boxes (list): list of bounding boxes currently tracked
      iou_threshold (float): IOU threshold
      
      Returns:
      boxes (list): list of bounding boxes after nms 
      '''
      if len(boxes) == 0:
        return []

      if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

      pick = []

      x1 = np.array(list(map(float, boxes[:,0])))
      y1 = np.array(list(map(float, boxes[:,1])))
      x2 = np.array(list(map(float, boxes[:,2])))
      y2 = np.array(list(map(float, boxes[:,3])))

      area = (x2 - x1 + 1) * (y2 - y1 + 1)
      idxs = np.argsort(y2)

      while len(idxs) > 0:

        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        overlap = (w * h) / area[idxs[:last]]

        idxs = np.delete(idxs, np.concatenate(([last],
          np.where(overlap > iou_threshold)[0])))

      return boxes[pick] #.astype("float")
        
    def format_detection_bbox(self, image, bboxes):
      '''
      Re-format object detection output for object tracking
      
      Args:
      image (np.ndarray): cv2 image used for object detection
      bboxes (list): list of bounding boxes in left, top, right, bottom format
      
      Returns:
      detections (list): list of detections re-formated for object tracking
      '''
      boxes, scores, names = [], [], []
      for bbox in bboxes:
        if len(self.track_only) !=0 and self.YOLO_Classes[int(bbox[5])] in self.track_only or len(self.track_only) == 0:
          boxes.append([bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]])
          scores.append(bbox[4])
          names.append(self.YOLO_Classes[int(bbox[5])])
      # Obtain all the detections for the given frame.
      boxes = np.array(boxes) 
      names = np.array(names)
      scores = np.array(scores)
      features = np.array(self.encoder(image, boxes))
      detections = [Detection(bbox, score, class_name, feature) for bbox, score, class_name, feature in zip(boxes, scores, names, features)]
      return detections
    
    def bbox_nms_format(self, bboxes):
      '''
      Bounding boxes from tracking re-formated for nms
      
      Args:
      bboxes (list): list of bounding boxes from tracking
      
      Returns:
      updates_bboxes (list): list of bounding boxes to be sent for nms
      '''
      updates_bboxes = []
      x1, y1, x2, y2, score, classes, direction = [],[],[],[],[],[],[]
      for box in bboxes:
        x1.append(float(box[0]))
        y1.append(float(box[1]))
        x2.append(float(box[2]))
        y2.append(float(box[3]))
        score.append(box[4])
        classes.append(box[5])
        direction.append(box[6]) 
      updates_bboxes.extend([x1, y1, x2, y2, score, classes, direction]) 
      updates_bboxes = np.transpose(updates_bboxes)
      return updates_bboxes
    
    def update_tracker(self, detections):
      '''
      Update object tracker with the bounding boxes re-formated from above
      
      Args: 
      detections (list): list of detections re-formated for object tracking
      '''
      self.tracker.predict()
      self.tracker.update(detections)
       
    def tracked_vehicles(self):
      '''
      Update object tracker with the bounding boxes re-formated from above
      
      Args: 
      detections (list): list of detections re-formated for object tracking
      
      Returns: 
      track (DeepSort tracker object): yields tracklets of every object tracked in frame 
      '''
      for track in self.tracker.tracks:
          yield track
          
    def clear_memory(self):
      '''
      clear tracked objects memory 
      '''
      if len(self.memory) > 100:
        del self.memory[list(self.memory)[0]]
      
      
      


