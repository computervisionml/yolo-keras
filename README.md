# INTRODUCTION  
YOLOv4 Keras  
  
# INSTALL REQUIREMENTS
 ```bash
 # Install requirements
 pip install -r requirements.txt
 ```

 # WEIGHTS  
 ## Download the yolov4.weights and yolov4.h5 into the "weights" folder   
 * download yolov4.weights: [yolov4.weights](https://drive.google.com/open?id=1cewMfusmPjYWbrnuJRuKhPMwRe_b9PaT)
 * download yolov4.h5: [yolov4.h5](https://drive.google.com/file/d/1gkINdqfeek5agviJchHD1om5ViPSkNSs/view?usp=sharing)
 
 ## Setup ROIs  
 ```bash
 # STEP 1: Convert get a frame from the video stream
 python ROI\get_frame.py -video_path <path to video> -road_id <json key for the frame>

 # STEP 2: Draw line ROI 
 python ROI\line_roi.py <road_id mentioned in STEP 1> 1

 # Draw trapezoid ROI 
python ROI\trapezoid_roi <road_id mentioned in STEP 1> 1
 ```

 ## Test scripts for Traffic Violations  
 ```bash
# Test red signal jump
python tests\red_signal.py -video_path <path to video> -camera_id <camera ID> -road_id <road ID assigned during ROI setup>

# Test wrong direction
python tests\wrong_direction.py -video_path <path to video> -camera_id <camera ID> -road_id <road ID assigned during ROI setup> 

# Test no parking violation
python tests\no_parking_estimation.py -video_path <path to video> -camera_id <camera ID> -road_id <road ID assigned during ROI setup> 
 ```
 

