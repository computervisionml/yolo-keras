import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from PIL import Image
import cv2
import numpy as np
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
import CONFIG
from CONFIG import FLAGS
import psutil
import time
import os
import multiprocessing
from collections import namedtuple

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

Box = namedtuple("Box", "y_min x_min y_max x_max")
Point = namedtuple("Point", "y x")

class CheckROI:
    # function to define ROI regions and TFLite model interpreter 
    def __init__(self):
        self.camera_rois = CONFIG.camera_rois

    # function to check if detected object in inside ROI
    def is_instance_in_roi(self, camera_id, x_min, y_min, width, height, classId):
        """
        Checks if the instance is within the defined Region of Interest
        for the camera_id
        Return the camera_id or camera_id + BRTS or Non_BRTS if the object is
        within the ROI
        """
        decimal_places = 3  # Number of decimal to round off
        v_type = self.vehicle_type(classId)
        vehicle_data = [
            'motorcycle',
            'small-car',
            'medium-car',
            'large-car',
            'bus',
            'truck'
        ]
        if v_type in vehicle_data:
            # Get box centroid
            centroid_x, centroid_y = int(x_min + width // 2), int(y_min + height // 2)
            centroid = {
                "x": round(centroid_x, decimal_places),
                "y": round(centroid_y, decimal_places),
            }

            if camera_id in ['554', '4554'] and self.object_in_roi(self.camera_rois[camera_id], centroid):
                return camera_id
            elif camera_id in ['5554', '2553']:
                keys = [camera_id + '_BRTS', camera_id + '_Non_BRTS']
                for key in keys:
                    if self.object_in_roi(self.camera_rois[key], centroid):
                        return key

    # function to check if point in ROI
    def point_in_box(self, box, point):
        """Return true if point lies in box"""
        if (box.x_min <= point.x <= box.x_max) and (box.y_min <= point.y <= box.y_max):
            return True
        return False

    # function to check if centroid in ROI
    def object_in_roi(self, roi, centroid):
        """Convenience to convert dicts to the Point and Box."""
        target_center_point = Point(centroid["y"], centroid["x"])
        roi_box = Box(roi["y_min"], roi["x_min"], roi["y_max"], roi["x_max"])
        return self.point_in_box(roi_box, target_center_point)

    
    def get_traffic_density(self, camera_id, response):
        '''
        Function to get JSON response of traffic density
        '''
        # Iterate on rekognition labels. Enrich and prep them for storage in DynamoDB
        labels_on_watch_list = []
        td_data = {
            'camera_id': camera_id,
            'motorcycle': 0,
            'small-car': 0,
            'medium-car': 0,
            'large-car': 0,
            'bus': 0,
            'truck': 0
        }
        rois = []  # Region of Interests for the camera
        for label in response:
            lbl = label['id']
            conf = label['score']
            label['OnWatchList'] = False
            # Print labels and confidence to lambda console
            # print('{} .. conf %{:.2f}'.format(lbl, conf))
            # Check label watch list and trigger action
            if (conf >= 0.3):
                label['OnWatchList'] = True
                labels_on_watch_list.append(label)
            # Convert from float to decimal for DynamoDB
            label['Confidence'] = str(conf)
            x_min = label['left'] 
            y_min = label['top'] 
            width = (label['right'] - label['left']) 
            height = (label['bottom'] - label['top']) 
            roi = self.is_instance_in_roi(camera_id, x_min, y_min, width, height, lbl)
            if roi:
                v_type = self.vehicle_type(lbl)
                if v_type in td_data:
                    td_data[v_type] += 1
                    if roi not in rois:
                        rois.append(roi)
        if rois:
            td_data['rois'] = ",".join(rois)
        if len(labels_on_watch_list) > 0:
            print("Total number of vehicles detected in frame: ", len(labels_on_watch_list))
        return td_data

    def vehicle_type(self,lbl):
        '''
        Convert label to required format.
        Returns the vehicle type
        '''
        if lbl.upper() in ['MOTORCYCLE', 'MOTORBIKE', 'BICYCLE']:
            return 'motorcycle'
        elif lbl.upper() == 'CAR':
            return 'small-car'
        elif lbl.upper() == 'TRUCK':
            return 'truck'
        elif 'BUS' in lbl.upper():
            return 'bus'

    # Function to estimate vehicle/traffic density
    def estimate_traffic_density(self, cameraId, detections):
        #Using my logic with "is_instance_in_roi" function to get vehicle count
        boxes = []
        for bbox in detections:   
            boxes.append([bbox["left"].astype(int), bbox["top"].astype(int), bbox["right"].astype(int)-bbox["left"].astype(int), bbox["bottom"].astype(int)-bbox["top"].astype(int), bbox["id"]])
        # boxes = np.array(boxes)
        vehicle_density = {}
        vehicle_density["vehicle_count_brts"] = 0
        vehicle_density["vehicle_count_non_brts"] = 0
        vehicle_density["vehicle_count"] = 0
        for box in boxes:
            roi_cameraId = self.is_instance_in_roi(cameraId, box[0], box[1], box[2], box[3], box[4])
            if roi_cameraId is not None:
                if cameraId in ['5554', '2553']:
                    if roi_cameraId.find("_Non_BRTS")>0:
                        vehicle_density["vehicle_count_non_brts"]+=1
                    elif roi_cameraId.find("_BRTS")>0:
                        vehicle_density["vehicle_count_brts"]+=1
                elif cameraId in ['554', '4554']:
                    vehicle_density["vehicle_count"]+=1

        #Using "get_traffic_density" to verify.
        traffic_density = self.get_traffic_density(cameraId, detections)
        
        return vehicle_density, traffic_density

   

    

