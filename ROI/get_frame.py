# imports
import cv2
import numpy as np

import os
import json 
import sys
import argparse

if __name__ == "__main__":
    # fetch video source
    parser = argparse.ArgumentParser(description='parse the image locations')
    parser.add_argument('-video_path',
                        metavar='s',
                        type=str,
                        default='./samples/hubli_cam.mp4',
                        help='the rtsp video source')

    parser.add_argument('-road_id',
                        metavar='s',
                        type=str,
                        default='hubli_cam_1',
                        help='road id')

    args = parser.parse_args()
    video_path = args.video_path
    road_id = args.road_id
    vid = cv2.VideoCapture(video_path)
    frame_skip = 0
    while vid.isOpened():
        frame_skip += 1
        if frame_skip % 10 != 0:
            continue
        ret , original_frame = vid.read()
        cv2.imwrite(os.path.join("./ROI",road_id+".jpg"), original_frame)
        break
        

