from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from ALPR.LicensePlateClass import LicensePlate, LicensePlate_V2

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse
import os

def show_roi(road_name):
    polygons = get_roi(road_name)
    pts = np.array(polygons, np.int32)
    pts = pts.reshape((-1,1,2))
    return pts, polygons

def get_roi(road_name):
    with open('./ROI/line_config.json', 'r+') as f:
        data = json.load(f)
        polygons = data[road_name]['polygon']
        return polygons


if __name__ == '__main__': 
  
  # initialise objects
  detect_vehicles = DetectVehicles()
  license_plate = LicensePlate_V2(saved_model="OCR/best_accuracy.pth", cuda=False)
  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-image_path',
                      metavar='s',
                      type=str,
                      default='./samples/test1.jpg',
                      help='the rtsp video source')
  
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')
  
  args = parser.parse_args()
  image_path = args.image_path
  camera_id = args.camera_id
  file_name = os.path.basename(image_path)

  images = os.listdir("samples")
  valid_files = ['jpg','png']
  images = [item for item in images if any(ext in item for ext in valid_files)]
  for item in images:
    original_frame = cv2.imread(os.path.join("./samples",item))
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    bboxes = detect_vehicles.get_bbox(image_bytes)
    
    tracked_bboxes = []
    for bbox in bboxes:
        frame = original_frame[bbox[1]:bbox[3],bbox[0]:bbox[2]]
        h, w = frame.shape[:2]
        if w < 400:
          continue
        Image.fromarray(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)).save(os.path.join("./results", item))
        # plates = license_plate.fetch_plate_number(frame)
        # if not len(plates):
          # continue
        # print(plates)
        # tracked_bboxes.append(bbox+plates)
        
    # updated_bboxes = track_vehicles.bbox_nms_format(tracked_bboxes)
    # bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_vehicles.non_max_suppression_fast(updated_bboxes)
    # original_frame = utils_image.draw_track_bbox(original_frame, tracked_bboxes, detect_vehicles.YOLO_Classes, tracking=False)      
    # cv2.imwrite(os.path.join("results",file_name), original_frame)
    # Image.fromarray(cv2.cvtColor(original_frame,cv2.COLOR_BGR2RGB)).show()