import tensorflow as tf

from PIL import Image
import cv2
import numpy as np

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from tools import utils_image
from core import config
from core import eval
from core import models

import psutil
import time
import os
import multiprocessing
from collections import namedtuple
import argparse

devices = tf.config.experimental.list_physical_devices('GPU')
if devices:
    tf.config.experimental.set_memory_growth(devices[0], True)


parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', type=str, help='input h5 model path', default='weights/yolov4.h5')
parser.add_argument('-i', '--image', type=str, help='input image file path', default='samples/hubli_cam_1.jpg')


args = parser.parse_args()
model_file_path = args.model
image_file_path = args.image
class_file_path = config.classes_path


anchors = config.anchors
class_names = config.classes_names

num_anchors = len(anchors)
num_classes = len(class_names)
class_mapping = dict(enumerate(class_names))
colors = utils_image.get_random_colors(len(class_names))
class_mapping = {class_mapping[key]: key for key in class_mapping}
model = models.YOLO()()
model.load_weights(model_file_path)


image = cv2.imread(image_file_path)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
new_image = utils_image.resize_image(image, config.image_input_shape)
new_image = np.array(new_image, dtype='float32')
new_image /= 255.

batch_size = 2
image_batch = []
for _ in range(batch_size):
  image_batch.append(new_image)
  
print("Batch shape: ",np.asarray(image_batch).shape)

while (1):
  
  # new_image = np.expand_dims(new_image, 0)
  start_time = time.time()
  feats = model.predict(image_batch)
  boxes, scores, classes = eval.yolo_eval(feats, anchors, len(class_names), (image.shape[0], image.shape[1]))
  
  print("Time to process batch: ", total_time)
  fps = 1.0 / (total_time)
  print("FPS: %.2f" % fps)
  print("CPU percentage used: ",psutil.cpu_percent())
  pid = os.getpid()
  py = psutil.Process(pid)
  print("Number of cpus: ",py.cpu_affinity())
  print("Number of threads: ",py.num_threads())
  memoryUse = py.memory_info()[0]/2.**30  # memory use in GB...I think
  print('Memory used in GB:', memoryUse)
  print("CPU Count",psutil.cpu_count())
  
  image = utils_image.draw_rectangle(image, boxes, scores, classes, class_names, colors, mode='pillow')
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
  cv2.namedWindow("img", cv2.WINDOW_NORMAL)
  cv2.imshow('img', image)
  cv2.waitKey(1)