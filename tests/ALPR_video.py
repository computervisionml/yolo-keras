from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from ALPR.LicensePlateClass import LicensePlate, LicensePlate_V2
from tracking.vehicle_tracking import TrackVehicles

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

def show_roi(road_name):
    polygons = get_roi(road_name)
    pts = np.array(polygons, np.int32)
    pts = pts.reshape((-1,1,2))
    return pts, polygons

def get_roi(road_name):
    with open('./ROI/line_config.json', 'r+') as f:
        data = json.load(f)
        polygons = data[road_name]['polygon']
        return polygons


if __name__ == '__main__': 
  
  # initialise objects
  detect_vehicles = DetectVehicles()
  license_plate = LicensePlate_V2(saved_model="OCR/best_norm_ED.pth")
  track_vehicles = TrackVehicles()
  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-video_path',
                      metavar='s',
                      type=str,
                      default='rtsp://admin:admin@123@112.133.197.90:2554/',
                      help='the rtsp video source')
  
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')
  
  args = parser.parse_args()
  video_path = args.video_path
  camera_id = args.camera_id
  file_name = os.path.basename(video_path)
  
  vid = cv2.VideoCapture(video_path)
  width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
  assert (width==int(vid.get(3))), "Not matching - width"
  height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
  assert (height==int(vid.get(4))), "Not matching - height"
  fps = int(vid.get(cv2.CAP_PROP_FPS))
  # print(fps)
  codec = cv2.VideoWriter_fourcc(*'XVID')
  output_path = os.path.join("results",file_name.split(".")[0]+".avi")
  out = cv2.VideoWriter(output_path, codec, fps, (width,height))
  
  already_tracked = []
  
  frame_skip = 0
  # checking frames on video
  while vid.isOpened():
    frame_skip += 1
    if frame_skip % 30 != 0:
      continue
    ret , original_frame = vid.read()
    # original_frame = cv2.resize(original_frame,(1080,800))
    if not ret:
      continue
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break  
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    bboxes = detect_vehicles.get_bbox(image_bytes)
    # cv2.imshow("frame",utils_image.draw_track_bbox(original_frame, bboxes,detect_vehicles.YOLO_Classes))
    # cv2.waitKey(1)
    # Image.fromarray(cv2.cvtColor(utils_image.draw_track_bbox(original_frame, bboxes,detect_vehicles.YOLO_Classes),cv2.COLOR_BGR2RGB)).show()
    detections = track_vehicles.format_detection_bbox(original_frame, bboxes)
    track_vehicles.update_tracker(detections)
    
    tracked_bboxes = []
    for track in track_vehicles.tracked_vehicles(): 
      if not track.is_confirmed() or track.time_since_update > 5:
        continue 
      bbox = track.to_tlbr() # Get the corrected/predicted bounding box
      class_name = track.get_class() #Get the class name of particular object
      tracking_id = track.track_id # Get the ID for the particular track
      index = track_vehicles.key_list[track_vehicles.val_list.index(class_name)] # Get predicted object index by object name
      
      bbox = bbox.tolist()
      bbox = [int(x) for x in bbox]
      
      frame = original_frame[bbox[1]:bbox[3],bbox[0]:bbox[2]]
      h, w = frame.shape[:2]
      if w < 100:
        continue
      if tracking_id not in already_tracked:
        plates = license_plate.fetch_plate_number(frame)
        if not len(plates):
          continue
        track.set_license_plate(plates[0])
      already_tracked.append(tracking_id)
      plates = [track.get_license_plate()] if track.get_license_plate() is not None else [""] 
      tracked_bboxes.append(bbox+ [tracking_id, index] + plates)
    
    print("Tracked boxes",tracked_bboxes)
    updated_bboxes = track_vehicles.bbox_nms_format(tracked_bboxes)
    bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_vehicles.non_max_suppression_fast(updated_bboxes)
    original_frame = utils_image.draw_track_bbox(original_frame, tracked_bboxes, detect_vehicles.YOLO_Classes, tracking=False)      
    out.write(original_frame.astype(np.uint8))
    original_frame = cv2.resize(original_frame,(640, 480))
    cv2.imshow("ANPR", original_frame.astype(np.uint8))
    cv2.waitKey(1)