from PIL import Image
import cv2
import numpy as np

from detection.vehicle_detection import DetectVehicles
from tracking.vehicle_tracking import TrackVehicles
from violations.vehicle_violation import TrafficViolation

from tools import utils_image

from collections import deque
import json
import time
import math
import datetime
import argparse
import shutil 
import psutil
import os

def show_roi(road_name):
    polygons = get_roi(road_name)
    pts = np.array(polygons, np.int32)
    pts = pts.reshape((-1,1,2))
    return pts, polygons

def get_roi(road_name):
    with open('./ROI/line_config.json', 'r+') as f:
        data = json.load(f)
        polygons = data[road_name]['polygon']
        return polygons


if __name__ == '__main__': 
  
  # initialise objects
  detect_vehicles = DetectVehicles()
  track_vehicles = TrackVehicles()
  track_violation = TrafficViolation()  
  
  # fetch video source
  parser = argparse.ArgumentParser(description='parse the image locations')
  parser.add_argument('-video_path',
                      metavar='s',
                      type=str,
                      default='./samples/hubli_cam.mp4', #rtsp://admin:admin@123@112.133.197.90:2554/
                      help='the rtsp video source')
  
  parser.add_argument('-camera_id',
                    metavar='s',
                    type=str,
                    default='5554',
                    help='the rtsp video source')
  
  parser.add_argument('-road_id',
                    metavar='s',
                    type=str,
                    default="hubli_cam_1",
                    help='the rtsp video source')
  
  args = parser.parse_args()
  video_path = args.video_path
  camera_id = args.camera_id
  road_id = args.road_id
  vid = cv2.VideoCapture(video_path)
  width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
  assert (width==int(vid.get(3))), "Not matching - width"
  height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
  assert (height==int(vid.get(4))), "Not matching - height"
  fps = int(vid.get(cv2.CAP_PROP_FPS))
  codec = cv2.VideoWriter_fourcc(*'XVID')
  # output_path = "detect-violation.avi"
  # out = cv2.VideoWriter(output_path, codec, fps, (width,height))
  
  pts, polygons = show_roi(road_id)
  polygon_xy1 = (int(polygons[0][0]), int(polygons[0][1]))
  polygon_xy2 = (int(polygons[1][0]), int(polygons[1][1]))
  roi_line = [polygon_xy1, polygon_xy2]
  # roi_line = [(int(0.15*width), int(0.8 * height)), (int(0.6*width), int(0.8 * height))]
  
  frame_skip = 0
  # checking frames on video
  while vid.isOpened():
    ret , original_frame = vid.read()
    if not ret:
      continue
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break  
    img_buffer = cv2.imencode('.jpg', original_frame)[1].tobytes()
    image_bytes = {}
    image_bytes["ImageBytes"] = img_buffer
    image_bytes["ContentType"]= 'image/jpg'
    image_bytes["CameraId"]= camera_id
    
    
    signal_copy = shutil.copy("violations/signal_status_main.json", "violations/signal_status.json") 
    with open("violations/signal_status.json","r") as json_file:
      signal_json = json.load(json_file)  

    signal_status = signal_json['color']

    start_time = time.time()
    bboxes = detect_vehicles.get_bbox(image_bytes)
    # tracking
    detections = track_vehicles.format_detection_bbox(original_frame, bboxes)
    track_vehicles.update_tracker(detections)
    if track_violation.track_violations(original_frame, camera_id, track_vehicles, roi_line, signal_status) is None:
      continue
    # use traffic_violations object
    tracked_bboxes = track_violation.track_violations(original_frame, camera_id, track_vehicles, roi_line, signal_status)
    
    total_time = time.time() - start_time
    print("Time to process batch: ", total_time)
    fps = 1.0 / (total_time)
    print("FPS: %.2f" % fps)
    track_vehicles.clear_memory()
    
    updated_bboxes = track_vehicles.bbox_nms_format(tracked_bboxes)
    bboxes = tracked_bboxes if np.asarray(tracked_bboxes).size == 0 else track_vehicles.non_max_suppression_fast(updated_bboxes)
    original_frame = utils_image.draw_track_bbox(original_frame, bboxes, track_vehicles.YOLO_Classes, tracking=True)
    cv2.line(original_frame, roi_line[0], roi_line[1], track_violation.color[signal_status], 2)
    image = cv2.resize(original_frame,(1080,720))
    cv2.imshow("Frame",image)
    cv2.waitKey(1)
    
    print("CPU percentage used: ",psutil.cpu_percent())
    pid = os.getpid()
    py = psutil.Process(pid)
    print("Number of cpus: ",py.cpu_affinity())
    print("Number of threads: ",py.num_threads())
    memoryUse = py.memory_info()[0]/2.**30  # memory use in GB...I think
    print('Memory used in GB:', memoryUse)
    print("CPU Count",psutil.cpu_count())